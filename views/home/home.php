<div class="content">
	<div class="container">
		<div class="top-banner5">
			<div class="row">
				<!-- <div class="col-md-3 col-sm-12 col-xs-12">
					<div class="bncat-slider5">
						<div class="wrap-item" data-pagination="false" data-navigation="true"
						data-itemscustom="[[0,1]]">
						<div class="list-bn-cat5"> -->
							<?php
							foreach ($this->categories as $key => $value) {
								?>
								<!-- <div class="item-bn-cat5">
									<div class="bncat-thumb5"> -->
										<!-- <a href="#"><img class="pulse-shrink"
										src="<?php echo ASSETS; ?>images/home5/smartphone.png" alt="" /></a> -->
									<!-- </div>
									<a href="<?php echo URL;?>products/category/<?php echo $value['id']?>"><?php echo $value['name']; ?></a>
								</div> -->
							<?php } ?>
						<!-- </div>
					</div>
				</div>
			</div> -->
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="banner-slider banner-slider5">
					<div class="wrap-item" data-autoplay="true" data-pagination="false"
					data-navigation="true" data-itemscustom="[[0,1]]">
					<div class="item-banner5">
						<div class="banner-thumb">
							<a href="#"><img src="<?php echo ASSETS; ?>images/home5/slide1.jpg" alt="" /></a>
						</div>
						<div class="banner-info left-to-right time-delay1">
							<h2 class="title60">Model</h2>
							<h3 class="title30 thin">Dress Women’s</h3>
							<h3 class="title30 color">$498.00</h3>
						</div>
					</div>
					<div class="item-banner5">
						<div class="banner-thumb">
							<a href="#"><img src="<?php echo ASSETS; ?>images/home5/slide2.jpg" alt="" /></a>
						</div>
						<div class="banner-info left-to-right time-delay1">
							<h2 class="title60">sale</h2>
							<h3 class="title30 thin">50% off</h3>
							<h3 class="title30">Bag & Shoes</h3>
						</div>
					</div>
					<div class="item-banner5">
						<div class="banner-thumb">
							<a href="#"><img src="<?php echo ASSETS; ?>images/home5/slide3.jpg" alt="" /></a>
						</div>
						<div class="banner-info text-center  top-to-bottom time-delay1">
							<h2 class="title60  color">70% off</h2>
							<h3 class="title30 thin">on save</h3>
							<h3 class="title30">tablet 24MB</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Top Banner -->

<div class="container">
	<div class="category-color color-brown banner-right">
		<div class="header-cat-color">
			<h2 class="title18">Solar Automatic Gate Opener</h2>
			<a href="<?php echo URL; ?>products/solar_automatic_gates" class="cat-color-link wobble-top">more</a>
		</div>
		<div class="content-cat-color">
			<div class="clearfix">
				<div class="banner-cat-color">
					<div class="banner-cat-color-thumb">
						<a href="#"><img src="<?php echo ASSETS; ?>images/gate-opener-kit.jpg" alt="" /></a>
					</div>
					<div class="banner-cat-color-info">
						<h2>Gate Opener Kits</h2>
						<a href="<?php echo URL; ?>products/solar_automatic_gates" class="pulse-grow">Shop now!</a>
					</div>
				</div>
				<div class="slider-cat-color">
					<div class="wrap-item" data-itemscustom="[[0,1],[480,2],[768,2],[1024,3],[1200,5]]"
					data-pagination="false" data-navigation="true">
					<?php
					// if($this->solarsystems)
					foreach ($this->solarautomatic as $value1) { ?>
						<div class="item-product item-product4">
							<div class="product-thumb">
								<a class="product-thumb-link">
									<img src="<?php echo URL . $value1["product_image"]; ?>" alt="" />
								</a>
								<a href="<?php echo URL; ?>products/quick_view/<?php echo $value1['id']; ?>"
									class="quickview-link plus fancybox.iframe">quick view</a>
									<div class="product-extra-link">
										<a href="#" class="addcart-link add-to-cart" data-name="<?php echo $value1['product_name']; ?>" data-price="<?php echo $value1['product_price']; ?>"><i class="fa fa-shopping-basket"
											aria-hidden="true"></i></a>
										</div>
									</div>
									<div class="product-info">
										<h3 class="product-title"><a href="<?php echo URL; ?>products/single_detail/<?php echo $value1['id']; ?>"><?php echo $value1['product_name']; ?></a></h3>
										<div class="product-price">
											<!-- <del><span>$400.00</span></del> -->
											<ins><span>&#8358;<?php echo $value1['product_price']; ?></span></ins>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
					<!-- End Slider Cat Color -->
				</div>
			</div>
			<!-- End Content Cat Color -->
		</div>
		<!-- End Category Color -->
		<div class="category-color color-blue banner-left">
			<div class="header-cat-color">
				<h2 class="title18">Solar Power Systems</h2>
				<a href="<?php echo URL; ?>products/solar_power_system" class="cat-color-link wobble-top">more</a>
			</div>
			<div class="content-cat-color">
				<div class="clearfix">
					<div class="banner-cat-color">
						<div class="banner-cat-color-thumb">
							<a href="#"><img src="<?php echo ASSETS; ?>images/power-system.jpeg" alt="" /></a>
						</div>
						<div class="banner-cat-color-info">
							<h2>Solar System</h2>
							<a href="<?php echo URL; ?>products/solar_automatic_gates" class="pulse-grow">Shop now!</a>
						</div>
					</div>
					<div class="slider-cat-color">
						<div class="wrap-item" data-itemscustom="[[0,1],[480,2],[768,2],[1024,3],[1200,5]]"
						data-pagination="false" data-navigation="true">
						<?php foreach ($this->solarsystems as $value2) { ?>
							<div class="item-product item-product4">
								<div class="product-thumb">
									<a class="product-thumb-link">
										<img src="<?php echo URL . $value2["product_image"]; ?>" alt="" />
									</a>
									<a href="<?php echo URL; ?>products/quick_view/<?php echo $value2['id']; ?>"
										class="quickview-link plus fancybox.iframe">quick view</a>
										<div class="product-extra-link">
											<a href="#" class="addcart-link add-to-cart" data-name="<?php echo $value2['product_name']; ?>" data-price="<?php echo $value2['product_price']; ?>"><i class="fa fa-shopping-basket"
												aria-hidden="true"></i></a>
											</div>
										</div>
										<div class="product-info">
											<h3 class="product-title"><a href="<?php echo URL; ?>products/single_detail/<?php echo $value2['id']; ?>"><?php echo $value2['product_name']; ?></a></h3>
											<div class="product-price">
												<!-- <del><span>$400.00</span></del> -->
												<ins><span>&#8358;<?php echo $value2['product_price']; ?></span></ins>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
						<!-- End Slider Cat Color -->
					</div>
				</div>
				<!-- End Content Cat Color -->
			</div>
			<!-- End Category Color -->

			<div class="category-color color-green banner-right">
				<div class="header-cat-color">
					<h2 class="title18">Lightings</h2>
					<a href="<?php echo URL; ?>products/lightings" class="cat-color-link wobble-top">more</a>
				</div>
				<div class="content-cat-color">
					<div class="clearfix">
						<div class="banner-cat-color">
							<div class="banner-cat-color-thumb">
								<a href="#"><img src="<?php echo ASSETS; ?>images/lightings.jpg" alt="" /></a>
							</div>
							<div class="banner-cat-color-info">
								<h2>Lightings</h2>
								<a href="<?php echo URL; ?>products/lightings" class="pulse-grow">Shop now!</a>
							</div>
						</div>
						<div class="slider-cat-color">
							<div class="wrap-item" data-itemscustom="[[0,1],[480,2],[768,2],[1024,3],[1200,5]]"
							data-pagination="false" data-navigation="true">
							<?php foreach ($this->lightings as $value3) { ?>

								<div class="item-product item-product4">
									<div class="product-thumb">
										<a class="product-thumb-link">
											<img src="<?php echo URL . $value3["product_image"]; ?>" alt="" />
										</a>
										<a href="<?php echo URL; ?>products/quick_view/<?php echo $value3['id']; ?>"
											class="quickview-link plus fancybox.iframe">quick view</a>
											<div class="product-extra-link">
												<a href="#" class="addcart-link add-to-cart" data-name="<?php echo $value3['product_name']; ?>" data-price="<?php echo $value3['product_price']; ?>"><i class="fa fa-shopping-basket"
													aria-hidden="true"></i></a>
												</div>
											</div>
											<div class="product-info">
												<h3 class="product-title"><a href="<?php echo URL; ?>products/single_detail/<?php echo $value3['id']; ?>"><?php echo $value3["product_name"];?></a></h3>
												<div class="product-price">
													<!-- <del><span>$400.00</span></del> -->
													<ins><span>&#8358;<?php echo $value3["product_price"];?></span></ins>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
							<!-- End Slider Cat Color -->
						</div>
					</div>
					<!-- End Content Cat Color -->
				</div>
				<!-- End Category Color -->
				<div class="category-color color-purple banner-left">
					<div class="header-cat-color">
						<h2 class="title18">Security Gadgets</h2>
						<a href="<?php echo URL; ?>products/security_gadgets" class="cat-color-link wobble-top">more</a>
					</div>
					<div class="content-cat-color">
						<div class="clearfix">
							<div class="banner-cat-color">
								<div class="banner-cat-color-thumb">
									<a href="#"><img src="<?php echo ASSETS; ?>images/cctv.png" alt="" /></a>
								</div>
								<div class="banner-cat-color-info">
									<h2>CCTV</h2>
									<a href="<?php echo URL; ?>products/security_gadgets" class="pulse-grow">Shop now!</a>
								</div>
							</div>
							<div class="slider-cat-color">
								<div class="wrap-item" data-itemscustom="[[0,1],[480,2],[768,2],[1024,3],[1200,5]]"
								data-pagination="false" data-navigation="true">
								<?php foreach ($this->security_gadgets as $value4) { ?>

									<div class="item-product item-product4">
										<div class="product-thumb">
											<a class="product-thumb-link">
												<img src="<?php echo URL . $value4["product_image"]; ?>" alt="" />
											</a>
											<a href="<?php echo URL; ?>products/quick_view/<?php echo $value4['id']; ?>"
												class="quickview-link plus fancybox.iframe">quick view</a>
												<div class="product-extra-link">
													<a href="#" class="addcart-link add-to-cart" data-name="<?php echo $value4['product_name']; ?>" data-price="<?php echo $value4['product_price']; ?>"><i class="fa fa-shopping-basket"
														aria-hidden="true"></i></a>
													</div>
												</div>
												<div class="product-info">
													<h3 class="product-title"><a href="<?php echo URL; ?>products/single_detail/<?php echo $value4['id']; ?>"><?php echo $value4["product_name"];?></a></h3>
													<div class="product-price">
														<!-- <del><span>$400.00</span></del> -->
														<ins><span>&#8358;<?php echo $value4["product_price"];?></span></ins>
													</div>
												</div>
											</div>
										<?php } ?>
									</div>
								</div>
								<!-- End Slider Cat Color -->
							</div>
						</div>
						<!-- End Content Cat Color -->
					</div>
					<!-- End Category Color -->

				</div>
			</div>
		</div>
