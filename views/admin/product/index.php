<?php
$product_model = new Products_Model();
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Manage Products</h3>
                    <span class="pull-right">
                        <button class="btn btn-primary" data-target="#new_product" data-toggle="modal">
                            Add Product
                        </button>
                    </span>
                </div>
                <div class="box-body">
                    <table id="data_table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Category</th>
                                <th>Parent Category</th>
                                <th>Product Name</th>
                                <th>Product Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            $products = $this->allProducts;
                            foreach ($products as $product) {
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <?php
                                    $category_infos = $product_model->categoryInfo($product['cat_id']);
                                    foreach ($category_infos as $cat) {
                                        ?>
                                        <td><?php echo $cat['name']; ?> </td>
                                        <?php
                                        $p_catinfo = $product_model->p_categoryInfo($cat['parent_cat']);
                                        foreach ($p_catinfo as $p_cat) { ?>
                                        <td><?php echo $p_cat['name']; ?> </td>
                                        <?php } ?>
                                    <?php } ?>
                                    <td><?php echo $product['product_name']; ?> </td>
                                    <td><img src="<?php echo URL . $product["product_image"]; ?>" alt="Image" style="height: 70px;"> </td>
                                    <td>
                                        <button onclick="edit_Product('<?php echo $product['id']; ?>');" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></button>
                                        <!--<button onclick="view_Product('<?php echo $product['id']; ?>');" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></button>-->
                                        <button onclick="delete_Product('<?php echo $product['id']; ?>');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <?php $count++; ?>
                            <?php }
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

<!-- Add Apartment Modal -->
<div id="new_product" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Product</h4>
            </div>
            <div class="modal-body">

                <form role="form" enctype="multipart/form-data" method="post" action="<?php echo URL;?>products/createProduct">
                    <div class="box-body">

                      <div class="form-group">
                          <label for="image">Image </label>
                          <input type="file" class="form-control" placeholder="Upload product image" name="product_image" accept="image/*">
                      </div>

                        <div class="form-group">
                            <label>Product Category</label>
                            <select class="form-control cat_id" name="cat_id" required="" onchange="getParentID()">
                              <option value="" disabled selected>Select category</option>
                                <?php
                                $categories = $this->categories;
                                foreach ($categories as $category) {
                                    ?>
                                    <option value="<?php echo $category['id']; ?>">
                                        <?php echo $category['name']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group" style="display: none;">
                          <select name="parent_cat" id="parent_cat">
                            <option value="" selected></option>
                          </select>
                        </div>

                        <div class="form-group">
                            <label for="name">Name </label>
                            <input type="text" class="form-control" placeholder="Enter Product Name" name="product_name" required="">
                        </div>

                        <div class="form-group">
                            <label for="name">Price </label>
                            <input type="text" class="form-control" placeholder="Enter Product Price" name="product_price" required="">
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea  class="form-control" placeholder="Enter Description" name="description"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="more_info">Information</label>
                            <textarea  class="form-control" placeholder="Enter Information" name="more_info"></textarea>
                        </div>

                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<div id="edit_product" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Product</h4>
            </div>
            <div class="modal-body" id="loadEditPage"></div>
        </div>
    </div>
</div>

<div id="delete_product" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Product</h4>
            </div>
            <div class="modal-body">
                <button class="btn btn-primary delete">Yes</button>
                <button class="btn btn cancel">No</button>
            </div>
        </div>
    </div>
</div>

<?php
require 'script.php';
?>

<script>
    $(function () {
        $("#data_table").DataTable();
    });
</script>
