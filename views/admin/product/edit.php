<?php
$edit_data = $this->prod_data;
foreach ($edit_data as $value) {
    ?>

    <form role="form" method="post" enctype="multipart/form-data" action="<?php echo URL;?>products/editProduct/<?php echo $value["id"]; ?>">
        <div class="box-body">

          <div class="form-group">
              <label for="image">Image </label>
              <input type="file" class="form-control" placeholder="Upload product image" name="product_image" accept="image/*">
              <!-- <img src="<?php echo URL . $value['product_image']; ?>" alt=""> -->
          </div>

            <div class="form-group">
                <label>Product Category</label>
                <select class="form-control cat_id" name="cat_id" required="">
                    <?php
                    $categories = $this->categories;
                    foreach ($categories as $category) {
                        ?>
                        <option value="<?php echo $category['id']; ?>"
                                <?php if ($category['id'] == $value['cat_id']) echo 'selected'; ?>>
                                    <?php echo $category['name']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label for="name">Name </label>
                <input type="text" class="form-control" placeholder="Enter Product Name" name="product_name" required=""  value="<?php echo $value['product_name']; ?>">
            </div>

            <div class="form-group">
                <label for="name">Price </label>
                <input type="text" class="form-control" placeholder="Enter Product Price" name="product_price" required=""  value="<?php echo $value['product_price']; ?>">
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea  class="form-control" placeholder="Enter Description" name="description"><?php echo $value['description']; ?></textarea>
            </div>

            <div class="form-group">
                <label for="more_info">Information</label>
                <textarea  class="form-control" placeholder="Enter Information" name="more_info"><?php echo $value['more_info']; ?></textarea>
            </div>

        </div><!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>

<?php } ?>
