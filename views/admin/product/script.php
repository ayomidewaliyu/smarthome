<script>
    function edit_Product(id) {
        $.get("<?php echo URL;?>products/updateProduct/" + id, function (resp) {
            $("#loadEditPage").html(resp);
            $('#edit_product').modal('show');
//            console.log("Worked");
        });
    }

    function delete_Product(id) {
        $("#delete_product").modal('show');
        $(".delete").click(function () {
            $.get("<?php echo URL;?>products/deleteProduct/" + id, function (resp) {
                alert("Deleted");
                $("#delete_product").modal('hide');
                location = "<?php echo URL;?>products/allProducts";
            });
        });
        $(".cancel").click(function () {
            $("#delete_product").modal("hide");
        })
    }

    function getParentID(){
      var cat_id = $(".cat_id").val();
      $.ajax({
        url: "<?php echo URL; ?>products/get_parent_cat/" + cat_id,
        method: "GET",
        dataType: "json",
        success: function(result){
          // console.log(result[0].parent_cat);
          console.log($("#parent_cat:first"));
          $("#parent_cat option:first").val(result[0].parent_cat);
        }
      })
    }

    $(function(){
      $(".process").submit(function(e){
        e.preventDefault();
        console.log($(this).serialize());
      })
    })

//    function editProduct(id, event) {
//        var postData = {
//            cat_id: $(".cat_id").val(),
//            product_name: $(".product_name").val(),
//            product_price: $(".product_price").val(),
//            product_image: $(".product_image").val(),
//            description: $(".desc").val(),
//            more_info: $(".more_info").val()
//        }
//
//        var valid = true,
//                message = '';
//
//        $('.editProduct input').each(function () {
//            var $this = $(this);
//
//            if (!$this.val()) {
//                var inputName = $this.attr('name');
//                valid = false;
//                message += 'Please enter your ' + inputName + '\n';
//            }
//        });
//
//        if (!valid) {
//            alert(message);
//            // return false;
//            event.preventDefault();
//        } else {
//            $.ajax({
//                type: 'POST',
//                url: "<?php echo URL;?>products/editProduct/" + id,
//                data: postData,
//                success: function (data) {
//                    location = "<?php echo URL;?>products/allProducts";
//                },
//                error: function () {}
//            });
//        }
//    }
</script>
