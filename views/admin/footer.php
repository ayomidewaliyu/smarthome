</div>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    <strong>Copyright &copy; <?php echo date("Y"); ?> <a  href="#" target="_blank"> Waliyullah Roji</a>.</strong> All rights reserved.
</footer>

<script src="<?php echo ADMIN_ASSETS; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo ADMIN_ASSETS; ?>js/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo ADMIN_ASSETS; ?>js/app.min.js"></script>
<script src="<?php echo ADMIN_ASSETS; ?>js/icheck.min.js"></script>
