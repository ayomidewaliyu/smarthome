<script>
    function delete_Message(id) {
        $("#delete_message").modal('show');
        $(".delete").click(function () {
            $.get("<?php echo URL;?>contact/deleteMessage/" + id, function (resp) {
                alert("Deleted");
                $("#delete_message").modal('hide');
                location = "<?php echo URL;?>contact/messages/";
//                window.reload;
            });
        });
        $(".cancel").click(function () {
            $("#delete_message").modal("hide");
        })
    }

    function edit_Message(id) {
        $.get("<?php echo URL;?>contact/messageView/" + id, function (resp) {
            $("#loadEditPage").html(resp);
            $('#view_message').modal('show');
        });
    }
</script>
