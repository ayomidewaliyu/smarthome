<?php
$edit_data = $this->message_data;
foreach ($edit_data as $value) {
    ?>

    <div class="col-md-6">
        <!-- Box Comment -->
        <div class="box box-widget">
            <div class="box-header with-border">
                <div class="user-block">
                    <span class="username"><a href="#"><?php echo $value["name"]; ?></a></span>
                    <span class="username"><a href="#"><?php echo $value["email"]; ?></a></span>
                    <span class="description">Sent - <?php echo $value["date"]; ?></span>
                </div><!-- /.user-block -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <h4><?php echo $value["subject"]; ?></h4>
                <!-- post text -->
                <p><?php echo $value["message"]; ?></p>

            </div><!-- /.box -->
        </div>

    <?php } ?>