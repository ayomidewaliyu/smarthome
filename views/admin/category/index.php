
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Product Categories</h3>
                    <span class="pull-right">
                        <button class="btn btn-primary" data-target="#new_category" data-toggle="modal">
                            Add Category
                        </button>
                    </span>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped" id="data_table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Parent Category</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            $categories = $this->categories;
                            $cat_model = new Category_Model();
                            foreach ($categories as $category) {
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $category['name']; ?> </td>
                                    <?php
                                    $p_cat_info = $cat_model->parent_categoryInfo($category['parent_cat']);
                                     foreach ($p_cat_info as $p_cat) {
                                         ?>
                                         <td><?php echo $p_cat['name']; ?> </td>
                                     <?php } ?>
                                    <td>
                                        <button onclick="edit_Category('<?php echo $category['id']; ?>');" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></button>
                                        <button onclick="delete_Category('<?php echo $category['id']; ?>');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                <?php $count++; ?>
                            <?php }
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

<!-- Add Apartment Modal -->
<div id="new_category" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Category</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" class="process">
                    <div class="box-body">

                        <div class="form-group">
                            <label for="name">Parent Category</label>
                            <select name="parent_cat" required="" class="form-control">
                              <?php foreach ($this->parent_cats as $key => $value) { ?>
                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                              <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="name">Category Name</label>
                            <input type="text" class="form-control" placeholder="Enter category name" name="name" required="">
                        </div>

                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<div id="edit_category" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Category</h4>
            </div>
            <div class="modal-body" id="loadEditPage"></div>
        </div>
    </div>
</div>

<div id="delete_category" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Category</h4>
            </div>
            <div class="modal-body">
                <!-- <p><strong>Make sure you update lease info before deleting</strong></p> -->
                <button class="btn btn-primary delete">Yes</button>
                <button class="btn btn cancel">No</button>
            </div>
        </div>
    </div>
</div>

<?php
require 'script.php';
?>

<script>
    $(function () {
        $("#data_table").DataTable();
    });
</script>
