<?php
$edit_data = $this->cat_data;
foreach ($edit_data as $value) {
    ?>

    <form role="form" method="post" class="process">
        <div class="box-body">

          <div class="form-group">
              <label for="name">Parent Category</label>
              <select name="parent_cat" required="" class="form-control parent_cat">
                <?php foreach ($this->parent_cats as $key => $value2) { ?>
                  <option value="<?php echo $value2['id']; ?>" <?php if($value2['id'] === $value['parent_cat']) echo "selected"; ?>><?php echo $value2['name']; ?></option>
                <?php } ?>
              </select>
          </div>

            <div class="form-group">
                <label for="name">Category Name</label>
                <input type="text" class="form-control name" placeholder="Enter category name" name="name" required="" value="<?php echo $value["name"]; ?>">
            </div>

        </div><!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary" onclick="editCategory(<?php echo $value['id']; ?>, event)">Submit</button>
        </div>
    </form>

<?php } ?>
