<script>
    function edit_Category(id) {
        $.get("<?php echo URL;?>parent_category/editParentCatPage/" + id, function (resp) {
            $("#loadEditPage").html(resp);
            $('#edit_category').modal('show');
//            console.log("Worked");
        });
    }

    function delete_Category(id) {
        $("#delete_category").modal('show');
        $(".delete").click(function () {
            $.get("<?php echo URL;?>parent_category/deleteParentCategory/" + id, function (resp) {
                alert("Deleted");
                $("#delete_category").modal('hide');
                location = "<?php echo URL;?>parent_category";
//                window.reload;
            });
        });
        $(".cancel").click(function () {
            $("#delete_category").modal("hide");
        })
    }

    function editCategory(id) {
        var postData = {
            name: $(".name").val()
        }

        var valid = true,
                message = '';

        $('.editCategory input').each(function () {
            var $this = $(this);

            if (!$this.val()) {
                var inputName = $this.attr('name');
                valid = false;
                message += 'Please enter your ' + inputName + '\n';
            }
        });

        if (!valid) {
            alert(message);
            // return false;
            event.preventDefault();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?php echo URL;?>parent_category/updateParentCat/" + id,
                data: postData,
                success: function (data) {
                    // $("#edit_building").modal("hide");
                    location = "<?php echo URL;?>parent_category";
                },
                error: function () {}
            });
        }
    }

    $(function () {
        $(".process").submit(function (e) {
            e.preventDefault();
            var postData = $(this).serialize();
            var url = "<?php echo URL;?>parent_category/addParentCat";
            console.log(postData);

            var valid = true,
                    message = '';

            $('form input').each(function () {
                var $this = $(this);

                if (!$this.val()) {
                    var inputName = $this.attr('name');
                    valid = false;
                    message += 'Please enter your ' + inputName + '\n';
                }
            });

            if (!valid) {
                alert(message);
                return false;
            } else {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: postData,
                    success: function (data) {
                        // $("#new_building").modal("hide");
                        location = "<?php echo URL; ?>parent_category";
                    },
                    error: function () {}
                });
                // return false;
            }
        });
    });
</script>
