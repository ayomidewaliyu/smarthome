<?php
$edit_data = $this->cat_data;
foreach ($edit_data as $value) {
    ?>

    <form role="form" method="post" class="process">
        <div class="box-body">

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control name" placeholder="Enter category name" name="name" required="" value="<?php echo $value["name"]; ?>">
            </div>

        </div><!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary" onclick="editCategory(<?php echo $value['id']; ?>, event)">Submit</button>
        </div>
    </form>

<?php } ?>
