<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Smart Home Admin Panel</title>
        <!-- Bootstrap 3.3.5 -->
        <link href="<?php echo ASSETS; ?>css/libs/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
        <!-- Font-awesome-->
        <link rel="stylesheet" href="<?php echo ADMIN_ASSETS; ?>css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo ADMIN_ASSETS; ?>css/styles.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo ADMIN_ASSETS; ?>css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo ADMIN_ASSETS; ?>css/dataTables.bootstrap.css">
        <link rel="stylesheet" href="<?php echo ADMIN_ASSETS; ?>css/_all-skins.min.css">
        <!-- js -->
        <!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script> -->
        <script type="text/javascript" src="<?php echo ASSETS; ?>js/jquery-2.1.4.min.js"></script>
        <!-- for bootstrap working -->
        <script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/bootstrap.min.js"></script>


    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>SMTHM</b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Smart</b> Home</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span>Admin
                                    </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header" style="height: auto !important;">
                                        <p>Admin
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-right">
                                            <button onclick="logout();" class="btn btn-danger btn-flat" id="logout_link">Sign out</button>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                        </ul>
                    </div>
                </nav>
            </header>

            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <ul class="sidebar-menu">
                        <li><a href="<?php echo URL; ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                        <li><a href="<?php echo URL; ?>products/allProducts"><i class="fa fa-object-group"></i> <span>Products</span></a></li>
                        <li><a href="<?php echo URL; ?>parent_category"><i class="fa fa-list"></i> <span>Parent Category</span></a></li>
                        <li><a href="<?php echo URL; ?>category"><i class="fa fa-list"></i> <span>Category</span></a></li>
                        <!-- <li><a href="<?php echo URL; ?>contact/messages"><i class="fa fa-inbox"></i> <span>Message</span></a></li> -->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <div class="content-wrapper">
