<div id="content">
	<div class="content-page page-product-hot-deal">
		<div class="container">
			<!-- <div class="banner-shop">
				<div class="banner-shop-thumb">
					<a href="#"><img src="<?php echo ASSETS; ?>images/shop/bn-grid-ajax.jpg" alt="" /></a>
				</div>
				<div class="banner-shop-info text-center">
					<h2>Best Of London Womenwear</h2>
					<p>Spring/Summer 2016</p>
				</div>
			</div> -->
			<!-- End Banner -->
			<div class="bread-crumb radius">
				<a href="<?php echo URL; ?>">Home</a>
				<span>
          SECURITY
					<?php
						// $uri = $_SERVER["REQUEST_URI"];
						// $id = array_slice(explode('/', $uri), -1)[0];
						// $prod_mod = new Products_Model();
						// $res = $prod_mod->p_categoryInfo($id);
						// echo $res[0]["name"];
						// echo $id;
						 ?>
				</span>
			</div>
			<!-- End Bread Crumb -->
			<!-- <div class="sort-pagi-bar clearfix">
				<div class="pull-left">
					<div class="sort-bar select-box">
						<label>Sort By:</label>
						<select>
							<option value="">position</option>
							<option value="">price</option>
						</select>
					</div>
				</div>
				<div class="pull-right">
					<div class="show-bar select-box">
						<label>Show:</label>
						<select>
							<option value="">20</option>
							<option value="">12</option>
							<option value="">24</option>
						</select>
					</div>
				</div>
			</div> -->
			<!-- End Sort PagiBar -->
			<div class="list-product-hot-deal">
				<div class="row">
					<?php foreach ($this->security_gadgets as $key => $value) { ?>
					<div class="col-md-3 col-sm-4 col-xs-12">
						<div class="item-product item-pro-hotdeal">
							<div class="product-thumb">
								<a href="#" class="product-thumb-link">
									<img src="<?php echo URL. $value["product_image"]; ?>" alt="">
								</a>
								<a href="<?php echo URL ?>products/quick_view/<?php echo $value["id"]; ?>" class="quickview-link plus fancybox.iframe"><span>quick view</span></a>

								<div class="product-extra-link">
									<a class="addcart-link add-to-cart" href="#" data-name="<?php echo $value['product_name']; ?>" data-price="<?php echo $value['product_price']; ?>"><i aria-hidden="true" class="fa fa-shopping-basket"></i></a>
								</div>
							</div>
							<div class="product-info">
								<h3 class="product-title"><a href="<?php echo URL ?>products/single_detail/<?php echo $value["id"]; ?>"><?php echo $value["product_name"]; ?></a></h3>
								<div class="info-pro-hotdeal">
									<!-- <div class="deal-percent">
										<span>66</span>
										<sup>%</sup>
									</div> -->
									<div class="product-price">
										<!-- <del><span>$400.00</span></del> -->
										<ins><span>&#8358;<?php echo $value["product_price"];?></span></ins>
									</div>
									<!-- <div class="product-rate">
										<div style="width:90%" class="product-rating"></div>
									</div> -->
									<div class="store-process" style="background: none;">
										<!-- <div class="percent-store" style="width:42%"></div> -->
										<!-- <span class="product-instock">24 PCs Left</span> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
			<div class="btn-loadmore"><a href="#"><i aria-hidden="true" class="fa fa-spinner fa-spin"></i></a></div>
		</div>
	</div>
</div>
<!-- End Content -->
