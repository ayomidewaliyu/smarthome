<div id="content">
			<div class="content-page">
				<?php foreach ($this->product_detail as $value) { ?>
				<div class="container">
					<div class="bread-crumb radius">
						<a href="<?php echo URL; ?>">Home</a> <span><?php echo $value["product_name"]; ?></span>
					</div>
					<!-- End Bread Crumb -->
					<div class="row">
						<div class="col-md-9 col-sm-8 col-col-xs-12">
							<?php
							// foreach ($this->product_detail as $value) { ?>
							<div class="product-detail bundle-detail border radius">
								<div class="row">
									<div class="col-md-5 col-sm-12 col-xs-12">
										<div class="detail-gallery">
											<div class="mid">
												<img src="<?php echo URL . $value["product_image"]; ?>" alt="" />
											</div>
										</div>
										<!-- End Gallery -->
									</div>
									<div class="col-md-7 col-sm-12 col-xs-12">
										<div class="detail-info">
											<h2 class="title-detail"><?php echo $value['product_name']; ?></h2>
											<p class="desc"><?php echo $value['description']; ?></p>
											<div class="available">
												<strong>Availability: </strong>
												<span class="in-stock">In Stock</span>
											</div>
											<div class="product-price">
												<span>&#8358;<?php echo $value['product_price']; ?></span>
											</div>
											<!-- <p class="price-config"><strong>Price as configured: $850.00</strong></p> -->

											<div class="detail-extralink">
												<div class="detail-qty border radius">
													<select name="quantity" class="form-control">
														<option value="1" selected>1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
													</select>
													<!-- <a href="#" class="qty-down"><i class="fa fa-caret-down"
															aria-hidden="true"></i></a>
													<span class="qty-val">1</span>
													<a href="#" class="qty-up"><i class="fa fa-caret-up"
															aria-hidden="true"></i></a> -->
												</div>
												<div class="product-extra-link2">
													<a class="addcart-link" href="#">Add to Cart</a>
												</div>
											</div>
										</div>
										<!-- Detail Info -->
									</div>
								</div>
								<div class="tab-detal toggle-tab">
									<div class="item-toggle-tab active">
										<h2 class="toggle-tab-title title14 radius border">Description</h2>
										<div class="toggle-tab-content">
											<div class="content-detail-tab">
												<div class="detail-tab-thumb">
													<img src="<?php echo URL . $value["product_image"]; ?>" alt="" />
												</div>
												<div class="detail-tab-info">
													<p class="desc">
														<?php echo $value['description']; ?>
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="item-toggle-tab">
										<h2 class="toggle-tab-title title14 radius border">More Information</h2>
										<div class="toggle-tab-content">
											<div class="content-detail-tab">
												<div class="detail-tab-thumb">
													<img src="<?php echo URL . $value["product_image"]; ?>" alt="" />
												</div>
												<div class="detail-tab-info">
													<p class="desc">
														<?php echo $value["more_info"]; ?>
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End Main Detail -->
						</div>
						<div class="col-md-3 col-sm-4 col-col-xs-12">
							<div class="sidebar sidebar-right">
								<div class="list-detail-adv">
									<div class="detail-adv">
										<a href="#"><img class="radius wobble-horizontal" alt=""
												src="<?php echo ASSETS; ?>images/shop/pro-adv1.jpg"></a>
									</div>
									<div class="detail-adv">
										<a href="#"><img class="radius wobble-horizontal" alt=""
												src="<?php echo ASSETS; ?>images/shop/pro-adv3.jpg"></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>

			</div>
		</div>
		<!-- End Content -->
