<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/bootstrap-theme.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/jquery.fancybox.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/jquery-ui.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/owl.transitions.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/owl.theme.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/animate.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/hover.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/flipclock.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/color-orange.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/theme.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/responsive.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/browser.css" media="all" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>

<div id="content">
  <div class="content-page">
    <div class="container">
      <div class="product-quickview">
        <div class="row">
          <?php foreach ($this->product_detail as $value) { ?>
          <div class="col-md-5 col-sm-6 col-xs-12">
            <div class="detail-gallery">
              <div class="mid">
                <img src="<?php echo URL . $value["product_image"]; ?>" alt=""/>
              </div>
              <!-- <div class="gallery-control">
                <a href="#" class="prev"><i class="fa fa-angle-left"></i></a>
                <div class="carousel">
                  <ul>
                    <li><a href="#" class="active"><img src="images/photos/homeware/8.jpg" alt=""/></a></li>
                    <li><a href="#"><img src="images/photos/homeware/3.jpg" alt=""/></a></li>
                    <li><a href="#"><img src="images/photos/homeware/2.jpg" alt=""/></a></li>
                    <li><a href="#"><img src="images/photos/homeware/4.jpg" alt=""/></a></li>
                    <li><a href="#"><img src="images/photos/homeware/5.jpg" alt=""/></a></li>
                    <li><a href="#"><img src="images/photos/homeware/7.jpg" alt=""/></a></li>
                  </ul>
                </div>
                <a href="#" class="next"><i class="fa fa-angle-right"></i></a>
              </div> -->
            </div>
            <!-- End Gallery -->
            <!-- <div class="detail-social">
              <ul class="list-social-detail list-inline-block">
                <li><a href="#" class="soci-fa soc-tumblr"><i class="fa fa-tumblr" aria-hidden="true"></i></a></li>
                <li><a href="#" class="soci-fa soc-facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#" class="soci-fa soc-twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#" class="soci-fa soc-print"><i class="fa fa-print" aria-hidden="true"></i></a></li>
                <li>
                  <div class="more-social">
                    <a class="soci-fa add-link soc-add" href="#"><i aria-hidden="true" class="fa fa-plus"></i><span>7</span></a>
                    <ul class="list-social-share list-none">
                      <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i><span>Youtute</span></a></li>
                      <li><a href="#"><i class="fa fa-linkedin"></i><span>linkedin</span></a></li>
                      <li><a href="#"><i class="fa fa-pinterest"></i><span>pinterest</span></a></li>
                      <li><a href="#"><i class="fa fa-google"></i><span>google</span></a></li>
                      <li><a href="#"><i class="fa fa-instagram"></i><span>instagram</span></a></li>
                      <li><a href="#"><i class="fa fa-flickr"></i><span>flickr</span></a></li>
                      <li><a href="#"><i class="fa fa-reddit"></i><span>reddit</span></a></li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div> -->
          </div>
          <div class="col-md-7 col-sm-6 col-xs-12">
            <div class="detail-info">
              <h2 class="title-detail"><?php echo $value['product_name']; ?></h2>
              <!-- <div class="product-rate">
                <div class="product-rating" style="width:90%"></div>
              </div> -->
              <div class="product-price">
                <ins><span>&#8358;<?php echo $value['product_price']; ?></span></ins>
              </div>
              <div class="available">
                <strong>Availability: </strong>
                <span class="in-stock">In Stock</span>
              </div>
              <!-- <a class="mail-to-friend" href="#">Email to a Friend</a> -->
              <!-- <div class="attr-detail attr-color">
                <div class="attr-title">
                  <strong><sup>*</sup>color:</strong><span class="current-color">White</span>
                </div>
                <ul class="list-filter color-filter">
                  <li><a href="#" data-color="Red"><span style="background:#ff596d"></span></a></li>
                  <li><a href="#" data-color="Yellow"><span style="background:#ffdb33"></span></a></li>
                  <li class="active"><a href="#" data-color="White"><span style="background:#ffffff"></span></a></li>
                  <li><a href="#" data-color="Orange"><span style="background:#ffbb51"></span></a></li>
                  <li><a href="#" data-color="Cyan"><span style="background:#80e6ff"></span></a></li>
                  <li><a href="#" data-color="Green"><span style="background:#38cf46"></span></a></li>
                  <li><a href="#" data-color="Purple"><span style="background:#ff8ff8"></span></a></li>
                </ul>
              </div> -->
              <!-- <div class="attr-detail attr-size">
                <div class="attr-title">
                  <strong><sup>*</sup>Size:</strong><span class="current-size">M</span>
                </div>
                <ul class="list-filter size-filter">
                  <li><a href="#">s</a></li>
                  <li class="active"><a href="#">m</a></li>
                  <li><a href="#">l</a></li>
                  <li><a href="#">xl</a></li>
                  <li><a href="#">2xl</a></li>
                </ul>
              </div> -->
              <div class="detail-extralink">
                <!-- <div class="detail-qty border radius">
                  <a href="#" class="qty-down"><i class="fa fa-caret-down" aria-hidden="true"></i></a>
                  <span class="qty-val">1</span>
                  <a href="#" class="qty-up"><i class="fa fa-caret-up" aria-hidden="true"></i></a>
                </div> -->
                <div class="product-extra-link2">
                  <a class="addcart-link" href="#">Add to Cart</a>
                  <!-- <a class="wishlist-link" href="#"><i aria-hidden="true" class="fa fa-heart"></i></a>
                  <a class="compare-link" href="#"><i aria-hidden="true" class="fa fa-refresh"></i></a> -->
                </div>
              </div>
              <div style="margin-top: 50px;">
                <h4>Product Description</h4>
                <p><?php echo $value['description']; ?></p>
              </div>
            </div>
            <!-- Detail Info -->
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>

<script src="<?php echo ASSETS; ?>js/cart.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/flipclock.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/theme.js"></script>
