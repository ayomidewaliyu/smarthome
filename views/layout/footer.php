<div id="footer">
	<div class="footer footer4">
		<div class="container">

			<div class="footer-list-box">
				<div class="row">
					<div class="col-md-5 col-sm-12 col-xs-12">
						<!-- <div class="newsletter-form footer-box">
						<h2 class="title14">Subscription</h2>
						<form>
						<input type="text" onblur="if (this.value=='') this.value = this.defaultValue"
						onfocus="if (this.value==this.defaultValue) this.value = ''"
						value="Place enter your email">
						<input type="submit" value="Subscription">
					</form>
				</div> -->
				<div class="social-footer footer-box">
					<h2 class="title14">Stay Connected</h2>
					<div class="list-social">
						<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
						<a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
			<!-- <div class="col-md-2 col-sm-3 col-xs-6">
			<div class="menu-footer-box footer-box">
			<h2 class="title14">How to Buy</h2>
			<ul class="list-unstyled">
			<li><a href="#">Create an Account</a></li>
			<li><a href="#">Making Payments</a></li>
			<li><a href="#">Delivery Options</a></li>
			<li><a href="#">Buyer Protection</a></li>
			<li><a href="#">New User Guide</a></li>
		</ul>
	</div>
</div> -->
<div class="col-md-4 col-sm-6 col-xs-6">
	<div class="menu-footer-box footer-box">
		<h2 class="title14">Customer Service</h2>
		<ul class="list-unstyled">
			<li><a href="#">Blog</a></li>
			<li><a href="#">About us</a></li>
			<li><a href="#">Contact us</a></li>
		</ul>
	</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
	<div class="contact-footer-box footer-box">
		<h2 class="title14">contact us</h2>
		<p class="formule-address">8901 Marmora Road, Glasgow, D04 89GR.</p>
		<p class="formule-phone">+1 800 559 6580<br>+1 504 889 9898</p>
		<p class="formule-email"><a href="mailto:email@demolink.org">email@demolink.org</a>
		</p>
	</div>
</div>
</div>
</div>
<!-- End Footer List Box -->
</div>
<div class="payment-method text-center">
	<a href="#" class="wobble-vertical"><img src="<?php echo ASSETS; ?>images/home4/pay1.png" alt="" /></a>
	<a href="#" class="wobble-vertical"><img src="<?php echo ASSETS; ?>images/home4/pay2.png" alt="" /></a>
	<a href="#" class="wobble-vertical"><img src="<?php echo ASSETS; ?>images/home4/pay3.png" alt="" /></a>
	<a href="#" class="wobble-vertical"><img src="<?php echo ASSETS; ?>images/home4/pay4.png" alt="" /></a>
</div>
<!-- End Payment -->

<div class="footer-copyright">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8 col-xs-12">
				<p class="copyright">SMARTHOME © <?php echo date("Y");?> smarthome.com.ng All rights reserved.</p>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<p class="designby">Design by: <a href="#">Bluescripts IT Institute</a></p>
			</div>
		</div>
	</div>
</div>
<!-- End Footer Copyright -->
</div>
</div>
<!-- End Footer -->
<a href="#" class="radius scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
</div>


<div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Cart</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<table class="show-cart table">

				</table>
				<div>Total price: &#8358;<span class="total-cart"></span></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Order now</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>

<script src="<?php echo ASSETS; ?>js/cart.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/flipclock.js"></script>
<!-- <script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/wow.js"></script> -->
<script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/jquery.jcarousellite.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/libs/jquery.elevatezoom.js"></script>

<script type="text/javascript" src="<?php echo ASSETS; ?>js/theme.js"></script>

<script>
$(function() {
	$("#place_order").click(function() {

		const product_in_cart = JSON.parse(localStorage.getItem('smart_shopping_cart'));

		const today = new Date();
		const date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
		const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
		const dateTime = date+' '+time;

		const shippingData = {
			fullname : $('.firstname').val() + " " + $('.lastname').val(),
			email : $('.email').val(),
			phone : $('.phone').val(),
			address : $('.address').val(),
			state : $('.state').val(),
			city : $('.city').val(),
			payment_method : $('input[name="payment_method"]:checked').val(),
			order_id: Date.now()
		}

		const order_details = product_in_cart.map((obj) => {
			return {
				product_quantity: obj.count,
				product_price: obj.price,
				product_name: obj.name,
				time: dateTime,
				order_id: Date.now()
			}
		});

		var paramJSON = JSON.stringify(order_details);

		$.ajax({
			url: '<?php echo URL; ?>checkout/saveShipping',
			method: 'POST',
			data: shippingData,
			success: function(response) {
				console.log(response);
				$.ajax({
					url: '<?php echo URL; ?>checkout/saveOrder/' + paramJSON,
					method: 'POST',
					success: function(result){
						console.log(result);
					}
				});
			}
		});

		// console.log(order_details);

	});
});
</script>

</body>

</html>
