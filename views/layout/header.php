<!DOCTYPE HTML>
<html lang="en-US">

<!-- Mirrored from demo.7uptheme.com/html/kuteshop/home-05.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 25 May 2019 04:51:20 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description"
	content="Kuteshop is new Html theme that we have designed to help you transform your store into a beautiful online showroom. This is a fully responsive Html theme, with multiple versions for homepage and multiple templates for sub pages as well" />
	<meta name="keywords" content="kuteshop,7uptheme" />
	<meta name="robots" content="noodp,index,follow" />
	<meta name='revisit-after' content='1 days' />
	<title><?php echo $this->title; ?></title>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/bootstrap-theme.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/jquery.fancybox.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/jquery-ui.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/owl.carousel.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/owl.transitions.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/owl.theme.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/jquery.mCustomScrollbar.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/animate.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/hover.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/libs/flipclock.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/color-orange.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/theme.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/responsive.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>css/browser.css" media="all" />
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
	<!-- Default theme -->
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
	<!-- Semantic UI theme -->
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
	<!-- Bootstrap theme -->
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
	<style>
	.show-cart li {
		display: flex;
	}
	.product-thumb-link {
		height: 200px !important;
	}
	.item-product.item-pro-hotdeal {
		/* border: 1px solid #0059a7 !important; */
	}
	.banner-cat-color-thumb img {
		height: 197px;
	}
	</style>
	<!-- <link rel="stylesheet" type="text/css" href="css/rtl.css" media="all"/> -->
</head>

<body style="background:#f4f4f4">
	<div class="wrap">
		<div id="header">
			<div class="header">
				<div class="main-header">
					<div class="container">
						<div class="row">
							<div class="col-md-3 col-sm-3 col-xs-12">
								<div class="logo logo4">
									<h1 class="hidden">Smart Home Automation</h1>
									<a href="index.html"><img src="<?php echo ASSETS; ?>images/home.jpg" alt="" /></a>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<h3>Smart Home</h3>
								<!-- <div class="smart-search smart-search4">
								<div class="select-category">
								<a class="category-toggle-link" href="#"><span>All Categories</span></a>
								<ul class="list-category-toggle list-unstyled">
								<li><a href="#">Computer &amp; Office</a></li>
								<li><a href="#">Elextronics</a></li>
								<li><a href="#">Jewelry &amp; Watches</a></li>
								<li><a href="#">Home &amp; Garden</a></li>
								<li><a href="#">Bags &amp; Shoes</a></li>
								<li><a href="#">Kids &amp; Baby</a></li>
							</ul>
						</div>
						<form class="smart-search-form">
						<input type="text" onblur="if (this.value=='') this.value = this.defaultValue"
						onfocus="if (this.value==this.defaultValue) this.value = ''"
						value="Search...">
						<input type="submit" value="">
					</form>
				</div> -->
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="check-cart">
					<div class="mini-cart-box">
						<a class="mini-cart-link" data-toggle="modal" data-target="#cart">
							<span class="mini-cart-icon"><i class="fa fa-shopping-basket"
								aria-hidden="true"></i></span>
								<!-- <span class="mini-cart-number">0</span> -->
								<span class="mini-cart-number total-count"></span>
							</a>

							<!-- <div class="mini-cart-content">
							<h2>(2) ITEMS IN MY CART</h2>
							<ul class="list-mini-cart-item list-unstyled">
							<li>
							<div class="mini-cart-edit">
							<a class="delete-mini-cart-item" href="#"><i
							class="fa fa-trash-o"></i></a>
							<a class="edit-mini-cart-item" href="#"><i
							class="fa fa-pencil"></i></a>
						</div>
						<div class="mini-cart-thumb">
						<a href="#"><img alt=""
						src="images/home1/mini-cart-thumb.png"></a>
					</div>
					<div class="mini-cart-info">
					<h3><a href="#">Burberry Pink &amp; black</a></h3>
					<div class="info-price">
					<span>$59.52</span>
					<del>$17.96</del>
				</div>
				<div class="qty-product">
				<span class="qty-down">-</span>
				<span class="qty-num">1</span>
				<span class="qty-up">+</span>
			</div>
		</div>
	</li>
	<li>
	<div class="mini-cart-edit">
	<a class="delete-mini-cart-item" href="#"><i
	class="fa fa-trash-o"></i></a>
	<a class="edit-mini-cart-item" href="#"><i
	class="fa fa-pencil"></i></a>
</div>
<div class="mini-cart-thumb">
<a href="#"><img alt=""
src="images/home1/mini-cart-thumb.png"></a>
</div>
<div class="mini-cart-info">
<h3><a href="#">Burberry Pink &amp; black</a></h3>
<div class="info-price">
<span>$59.52</span>
<del>$17.96</del>
</div>
<div class="qty-product">
<span class="qty-down">-</span>
<span class="qty-num">1</span>
<span class="qty-up">+</span>
</div>
</div>
</li>
</ul>
<div class="mini-cart-total">
<label>TOTAL</label>
<span>$24.28</span>
</div>
<div class="mini-cart-button">
<a class="mini-cart-view" href="#">view my cart </a>
<a class="mini-cart-checkout" href="#">Checkout</a>
</div>
</div> -->
</div>
<!-- End Mini Cart -->
<!-- <div class="wishlist-box">
<a href="#" class="wishlist-top-link"><i class="fa fa-heart-o"
aria-hidden="true"></i></a>
</div> -->
<!-- End Wishlist -->
<!-- <div class="checkout-box">
<a href="#" class="checkout-link"><i class="fa fa-lock"
aria-hidden="true"></i></a>
<ul class="list-checkout list-unstyled">
<li><a href="#"><i class="fa fa-user"></i> Account Info</a></li>
<li><a href="#"><i class="fa fa-heart-o"></i> Wish List</a></li>
<li><a href="#"><i class="fa fa-toggle-on"></i> Compare</a></li>
<li><a href="#"><i class="fa fa-key" aria-hidden="true"></i>Sign in</a></li>
<li><a href="#"><i class="fa fa-sign-in"></i> Checkout</a></li>
</ul>
</div> -->
<!-- End Check Out Box -->
</div>
</div>
</div>
<div class="header-nav5">
	<div class="row">
		<div class="col-md-3 hidden-sm hidden-xs">
			<div class="wrap-cat-icon wrap-cat-icon-hover wrap-cat-icon5">
				<h2 class="title14 title-cat-icon">Categories</h2>
				<div class="wrap-list-cat-icon">
					<ul class="list-cat-icon">
						<?php
						foreach ($this->categories as $key => $value) {
							?>
							<li><a href="<?php echo URL;?>products/category/<?php echo $value['id']?>"><span><?php echo $value['name']; ?></span></a>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<!-- End Main Header -->
</div>
</div>
<!-- End Header -->
