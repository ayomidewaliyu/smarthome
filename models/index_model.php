<?php

class Index_Model extends Model {

    function __construct() {
        parent::__construct();
    }

    function latestProducts() {
        return $this->db->select("SELECT * FROM products ORDER BY id ASC LIMIT 8");
    }

    function product_categories() {
        return $this->db->select("select * from parent_category");
    }

    public function solarAutomatic(){
      return $this->db->select("select * from products where parent_cat = 1 limit 8");
    }

    public function solarPower(){
      return $this->db->select("select * from products where parent_cat = 2 limit 8");
    }

    public function lightings(){
      return $this->db->select("select * from products where parent_cat = 3 limit 8");
    }

    public function security(){
      return $this->db->select("select * from products where parent_cat = 4 limit 8");
    }

}
