<?php

class Category_Model extends Model {

  function __construct() {
    parent::__construct();
  }

  public function saveCat() {
    $catData = array(
      'name' => $_POST['name'],
      'parent_cat' => $_POST['parent_cat']
    );

    $this->db->insert('product_category', $catData);
  }

  public function editCat($id) {
    $catData = array(
      'name' => $_POST['name'],
      'parent_cat' => $_POST['parent_cat']
    );

    $this->db->update('product_category', $catData, "id = " . $id);
  }

  // get parent category info
  public function parent_categoryInfo($id = '') {
    $param = array(
      ":categoryid" => $id,
    );
    $getinfo_query = $this->db->select("SELECT * FROM parent_category WHERE id = :categoryid", $param);
    return $getinfo_query;
  }

  public function deleteCat($id) {
    $this->db->delete('product_category', "id = " . $id);
  }

  public function allCategories() {
    return $this->db->select("select * from product_category");
  }

  public function allParentCat() {
    return $this->db->select("select * from parent_category");
  }

  public function getProdCat($id){
    return $this->db->select("select * from product_category where id = '$id'");

  }


}
