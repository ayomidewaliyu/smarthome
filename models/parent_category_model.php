<?php

class Parent_category_Model extends Model {

  function __construct() {
    parent::__construct();
  }

  public function saveParentCat() {
    $catData = array(
      'name' => $_POST['name']
    );

    $this->db->insert('parent_category', $catData);
  }

  public function editParentCat($id) {
    $catData = array(
      'name' => $_POST['name']
    );

    $this->db->update('parent_category', $catData, "id = " . $id);
  }

  public function deleteParentCat($id) {
    $this->db->delete('parent_category', "id = " . $id);
  }

  public function allParentCategories() {
    return $this->db->select("select * from parent_category");
  }

  public function getParentProdCat($id){
    return $this->db->select("select * from parent_category where id = '$id'");
  }
}
