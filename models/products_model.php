<?php

require "Upload.php";

class Products_Model extends Model {

  function __construct() {
    parent::__construct();
  }

  public function addProduct() {
    if (!empty($_FILES['product_image']['name'][0])) {
      try {
        $pix = $_FILES['product_image'];

        $pixExts = array('jpg', 'png', 'gif', 'jpeg');
        $uploadsDirectory = UPLOADS;
        $maxSize = 4000000;
        $uploadpix = new Upload($pix, $pixExts, $uploadsDirectory, $maxSize);
        $uploadpix->uploadFiles();
        $_POST['product_image'] = $uploadsDirectory . $uploadpix->getFileUrl();
      } catch (Exception $exc) {
        print_r($exc->getMessage());
      }
    } else {
      $_POST['product_image'] = URL . 'assets/images/close.png';
    }

    $productData = array(
      "parent_cat" => $_POST["parent_cat"],
      "cat_id" => $_POST["cat_id"],
      "product_name" => $_POST["product_name"],
      "product_price" => $_POST["product_price"],
      "product_image" => $_POST["product_image"],
      "description" => $_POST["description"],
      "more_info" => $_POST["more_info"],
      "date" => date("Y-m-d")
    );
    $this->db->insert("products", $productData);
    // print_r($productData);
  }

  public function editProd($id) {
    if (!empty($_FILES['product_image']['name'][0])) {
      try {
        $pix = $_FILES['product_image'];

        $pixExts = array('jpg', 'png', 'gif', 'jpeg');
        $uploadsDirectory = UPLOADS;
        $maxSize = 4000000;
        $uploadpix = new Upload($pix, $pixExts, $uploadsDirectory, $maxSize);
        $uploadpix->uploadFiles();
        $_POST['product_image'] = $uploadsDirectory . $uploadpix->getFileUrl();
      } catch (Exception $exc) {
        print_r($exc->getMessage());
      }
    } else {
      $_POST['product_image'] = URL . 'assets/images/close.png';
    }

    $productData = array(
      "parent_cat" => $_POST["parent_cat"],
      "cat_id" => $_POST["cat_id"],
      "product_name" => $_POST["product_name"],
      "product_price" => $_POST["product_price"],
      "product_image" => $_POST["product_image"],
      "description" => $_POST["description"],
      "more_info" => $_POST["more_info"],
      "date" => date("Y-m-d")
    );
    $this->db->update("products", $productData, "id =" . $id);
    // print_r($productData);
  }

  public function allProducts() {
    $products = $this->db->select("select * from products");
    return $products;
  }

  public function getProduct($id) {
    return $this->db->select("select * from products where id =" . $id);
  }

  public function getCategories() {
    return $this->db->select("select * from product_category");
  }

  // get category info
  public function categoryInfo($id = '') {
    $param = array(
      ":categoryid" => $id,
    );
    $getinfo_query = $this->db->select("SELECT * FROM product_category WHERE id = :categoryid", $param);
    return $getinfo_query;
  }

  // get category info
  public function p_categoryInfo($id = '') {
    $param = array(
      ":categoryid" => $id,
    );
    $getinfo_query = $this->db->select("SELECT * FROM parent_category WHERE id = :categoryid", $param);
    return $getinfo_query;
  }

  public function deleteProd($id) {
    $this->db->delete("products", "id = " . $id);
  }

  public function product_categories($id) {
    $parent_cat = $this->p_categoryInfo($id);
    $cat_name = $parent_cat[0]["name"];
    return $this->db->select("select * from products where parent_cat = '$id'");
  }

  function parent_categories() {
      return $this->db->select("select * from parent_category");
  }

  public function getParentID($cat_id){
    $fetch = $this->db->select("select parent_cat from product_category where id = '$cat_id'");
    echo json_encode($fetch);
  }

  public function solarAutomatic(){
    return $this->db->select("select * from products where parent_cat = 1");
  }

  public function solarPower(){
    return $this->db->select("select * from products where parent_cat = 2");
  }

  public function lightings(){
    return $this->db->select("select * from products where parent_cat = 3");
  }

  public function security(){
    return $this->db->select("select * from products where parent_cat = 4");
  }

}
