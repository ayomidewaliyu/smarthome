<?php

class Checkout_Model extends Model {

  function __construct() {
    parent::__construct();
  }

  public function saveOrder($data) {


    // print_r(json_decode($data));
    foreach (json_decode($data, true) as $value) {

      $this->db->insert('orders', $orderData = array(
        'product_name' => $value['product_name'],
        'product_price' => $value['product_price'],
        'product_quantity' => $value['product_quantity'],
        'order_id' => $value['order_id'],
        'time' => $value['time']
      ));

    }

  }

  function saveShippingDetail(){
    $shippingData = array(
      'fullname' => $_POST['fullname'],
      'email' => $_POST['email'],
      'phone' => $_POST['phone'],
      'address' => $_POST['address'],
      'state' => $_POST['state'],
      'city' => $_POST['city'],
      'payment_method' => $_POST['payment_method'],
      'order_id' => $_POST['order_id'],
      'date' => date("Y-m-d")
    );

    $this->db->insert('shipping', $shippingData);
    // echo json_encode($shippingData);
  }

  function parent_categories() {
    return $this->db->select("select * from parent_category");
  }

}
