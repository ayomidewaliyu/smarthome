<?php

class Index extends Controller
{

  function __construct()
  {
    parent::__construct();
    $this->loadModel('index');
  }

  public function index(){
    $this->view->categories = $this->model->product_categories();
    $this->view->solarsystems = $this->model->solarPower();
    $this->view->solarautomatic = $this->model->solarAutomatic();
    $this->view->lightings = $this->model->lightings();
    $this->view->security_gadgets = $this->model->security();

    $this->view->title = "Smart Home";
    $this->view->render('layout/header');
    $this->view->render('home/home');
    $this->view->render('layout/footer');
  }

  public function description(){
    $this->view->title = "Product Description";
    $this->view->render('layout/header');
    $this->view->render('products/single-product');
    $this->view->render('layout/footer');
  }

  public function checkout(){
    $this->view->title = "Product Description";
    $this->view->render('layout/header');
    $this->view->render('checkout/checkout');
    $this->view->render('layout/footer');
  }

  public function cart(){
    $this->view->title = "Product Description";
    $this->view->render('layout/header');
    $this->view->render('cart/cart');
    $this->view->render('layout/footer');
  }
}


 ?>
