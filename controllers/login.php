<?php

class Login extends Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->view->render('admin/login');
    }

    function authenticate() {
        $this->model->login();
    }

}
