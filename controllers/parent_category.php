
<?php

class Parent_category extends Controller {

  function __construct() {
    parent::__construct();
    Session::init();
  }

  public function index() {
    //        Session::init();
    if (isset($_SESSION["KGE_LOGGED_IN"])) {
      $this->view->parent_categories = $this->model->allParentCategories();

      $this->view->render("admin/header");
      $this->view->render("admin/category/parent_category");
      $this->view->render("admin/footer");
    } else {
      $this->view->render('admin/login');
    }
  }

  public function editParentCatPage($id) {
//        Session::init();
      if (isset($_SESSION["KGE_LOGGED_IN"])) {
          $this->view->cat_data = $this->model->getParentProdCat($id);

          $this->view->render("admin/category/edit_parent_cat");
      } else {
          $this->view->render('admin/login');
      }
  }


  public function addParentCat() {
    $this->model->saveParentCat();
  }

  public function updateParentCat($id) {
    $this->model->editParentCat($id);
  }

  public function deleteParentCategory($id) {
    $this->model->deleteParentCat($id);
  }
}
