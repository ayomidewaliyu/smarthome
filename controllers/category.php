<?php

class Category extends Controller {

    function __construct() {
        parent::__construct();
        Session::init();
    }

    public function index() {
//        Session::init();
        if (isset($_SESSION["KGE_LOGGED_IN"])) {
            $this->view->categories = $this->model->allCategories();
            $this->view->parent_cats = $this->model->allParentCat();

            $this->view->render("admin/header");
            $this->view->render("admin/category/index");
            $this->view->render("admin/footer");
        } else {
            $this->view->render('admin/login');
        }
    }

    public function editCategoryPage($id) {
//        Session::init();
        if (isset($_SESSION["KGE_LOGGED_IN"])) {
            $this->view->cat_data = $this->model->getProdCat($id);
            $this->view->parent_cats = $this->model->allParentCat();

            $this->view->render("admin/category/edit");
        } else {
            $this->view->render('admin/login');
        }
    }

    public function addCat() {
        $this->model->saveCat();
    }

    public function updateCat($id) {
        $this->model->editCat($id);
    }

    public function deleteCategory($id) {
        $this->model->deleteCat($id);
    }

}
