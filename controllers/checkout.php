<?php

class Checkout extends Controller {

  function __construct() {
    parent::__construct();
    $this->loadModel('products');
  }

  public function index() {
    $this->view->title = "Checkout";
    $this->view->categories = $this->model->parent_categories();

    $this->view->render("layout/header");
    $this->view->render("checkout/checkout");
    $this->view->render("layout/footer");
  }

  function saveOrder($data){
    $this->model->saveOrder($data);
  }

  function saveShipping(){
    $this->model->saveShippingDetail();
  }

}
