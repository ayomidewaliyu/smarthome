<?php

class Products extends Controller {

  function __construct() {
    parent::__construct();
    Session::init();
  }

  public function index() {
    $this->view->title = "Products";
    $this->view->products = $this->model->allProducts();
    $this->view->categories = $this->model->parent_categories();

    $this->view->render("layout/header");
    $this->view->render("products/products");
    $this->view->render("layout/footer");
  }

  public function solar_automatic_gates(){
    $this->view->title = "Solar Automatic Gate Opener";
    $this->view->solarautomatic = $this->model->solarAutomatic();
    $this->view->categories = $this->model->parent_categories();

    $this->view->render("layout/header");
    $this->view->render("products/categories/solar_automatic");
    $this->view->render("layout/footer");
  }

  public function solar_power_system(){
    $this->view->title = "Solar Power System";

    $this->view->solarsystems = $this->model->solarPower();
    $this->view->categories = $this->model->parent_categories();

    $this->view->render("layout/header");
    $this->view->render("products/categories/solar_power");
    $this->view->render("layout/footer");
  }

  public function security_gadgets(){
    $this->view->title = "Security Gadgets";

    $this->view->security_gadgets = $this->model->security();
    $this->view->categories = $this->model->parent_categories();

    $this->view->render("layout/header");
    $this->view->render("products/categories/security_gadgets");
    $this->view->render("layout/footer");
  }

  public function lightings(){
    $this->view->title = "Lightings";

    $this->view->lightings = $this->model->lightings();
    $this->view->categories = $this->model->parent_categories();


    $this->view->render("layout/header");
    $this->view->render("products/categories/lightings");
    $this->view->render("layout/footer");
  }

  public function single_detail($id) {
    $this->view->title = "Single Product Detail";

    $this->view->categories = $this->model->parent_categories();
    $this->view->product_detail = $this->model->getProduct($id);

    $this->view->render("layout/header");
    $this->view->render("products/single-product");
    $this->view->render("layout/footer");
  }

  public function quick_view($id) {
    // $this->view->title = "Product Description";

    $this->view->categories = $this->model->parent_categories();
    $this->view->product_detail = $this->model->getProduct($id);

    // $this->view->render("layout/header");
    $this->view->render("products/quick-view");
    // $this->view->render("layout/footer");
  }

  public function category($id){
    $this->view->title = "Products";
    // $this->view->title = "Products";

    $this->view->products_in_cat = $this->model->product_categories($id);
    $this->view->categories = $this->model->parent_categories();


    $this->view->render("layout/header");
    $this->view->render("products/products");
    $this->view->render("layout/footer");
  }

  public function allProducts() {
    //        Session::init();
    if (isset($_SESSION["KGE_LOGGED_IN"])) {
      $this->view->categories = $this->model->getCategories();
      $this->view->allProducts = $this->model->allProducts();

      $this->view->render("admin/header");
      $this->view->render("admin/product/index");
      $this->view->render("admin/footer");
    } else {
      $this->view->render('admin/login');
    }
  }

  function getAllProducts(){
    $this->model->allProducts();
  }

  public function updateProduct($id) {
    //        Session::init();
    if (isset($_SESSION["KGE_LOGGED_IN"])) {
      $this->view->prod_data = $this->model->getProduct($id);
      $this->view->categories = $this->model->getCategories();

      $this->view->render("admin/product/edit");
    } else {
      $this->view->render('admin/login');
    }
  }

  public function createProduct() {
    $this->model->addProduct();
    header("Location: " . URL . "products/allProducts");
  }

  public function editProduct($id) {
    $this->model->editProd($id);
    header("Location: " . URL . "products/allProducts");
  }

  public function deleteProduct($id) {
    $this->model->deleteProd($id);
  }

  public function get_parent_cat($cat_id){
    $this->model->getParentID($cat_id);
  }

}
