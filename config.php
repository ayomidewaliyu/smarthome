<?php

// Always provide a TRAILING SLASH (/) AFTER A PATH

define('URL', 'http://localhost/smarthome/');
define('ASSETS', 'http://localhost/smarthome/assets/');
define('ADMIN_ASSETS', 'http://localhost/smarthome/assets/admin/');
define('LIBS', 'libs/');
define('UPLOADS', 'uploads/');

define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_NAME', 'smarthome');
define('DB_USER', 'root');
define('DB_PASS', '');
